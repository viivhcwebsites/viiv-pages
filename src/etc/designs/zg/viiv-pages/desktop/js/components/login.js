
(function($, _) {
    "use strict";

    var api = {};


    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    // TOKEN JS
    var domain = window.location.hostname;
    var htmlTokenValue = window.location.hash;


    function sessionStorageSet() {
        sessionStorage.setItem('tokenPage', htmlTokenValue);
    }



    $( document ).ready( function() {
        if (( domain != "localhost" ) && (domain != "prod.cf5.author.gsk.com" ) && (domain != "preprod.cf5.author.gsk.com" )) {
        //if ((domain != "prod.cf5.author.gsk.com" )) {
            //alert(tokenValue);

            sessionStorage.setItem('tokenPage', '');
            if( tokenValue == htmlTokenValue ) {
                sessionStorageSet();
            }
            let dataToken = sessionStorage.getItem('tokenPage');
            console.log(dataToken);

            if( tokenValue != dataToken ) {

                $( "#header" ).html( "<br/>" );
                $( "#content" ).html( "<h1 class='h1-error'>This page contains promotional content and is only intended for Health Care Professionals. </h1>" );
            } else {
                $( "#header" ).show();
                $( "#content" ).show();
                window.location = "#token";
            }

        } else {
            $( "#header" ).show();
            $( "#content" ).show();
        }


        $('.abstract_buttons a').on('click', function(e){
            e.preventDefault();
        });
        $(".abstract_buttons").on('click', function(){
            var abstract_parent = $(this).parent();
            if ((abstract_parent.hasClass('active'))) {
                $(".abstract").removeClass('active');
                $(".abstract_content").slideUp();
            } else {
                $(".abstract").removeClass('active');
                $(".abstract_content").slideUp();
                abstract_parent.addClass('active');
                abstract_parent.find(".abstract_content").slideDown();
            }

        });
    } );


    // MOVIES JS ///

    function getVideoData(videoNumber) {
        var videoNumberNN = videoNumber + 1;
        var videoCount = parseInt($('.videos-container').attr('data-videocount'));
        $("video").each(function () { this.pause() });
        if(videoNumberNN == videoCount) {
            $('.videosNav .next').hide();
        }
        if(videoNumberNN == 1) {
            $('.videosNav .previous').hide();
        }
        $(".box-intro").show();
        $(".box-player").hide();

        //$('.box-intro').attr('data-movie', videoNumberNN).hide();
        $(".box-intro[data-movie="+videoNumberNN+"]").hide();
        $(".box-player[data-movie="+videoNumberNN+"]").show();
        //alert(videoNumberNN);

        console.log(videoNumberNN);
    }

    $('.box-intro').on('click', function(){
        var videoID = $(this).data('movie');
        getVideoData(videoID - 1);
    });
    $('.videosNav .next').on('click', function(){
        var videoIDnext = parseInt($(this).attr('data-videoNext'));
        //alert(videoIDnext);
        //getVideoData(videoIDnext);
    });
    $('.videosNav .previous').on('click', function(){
        var videoIDprev = parseInt($(this).attr('data-videoPrev'));
        //alert(videoIDprev);
        //getVideoData(videoIDprev);
    });


    getVideoData(0);

    // EXIT NOTIFICATION
    var elementUrl = "";

    $('.references h4').on('click', function() {
        var references = $(this).closest('.references');
        var referencesList = $(this).next('ol');
        if (references.hasClass('is-active')){
            references.removeClass('is-active');
            referencesList.slideUp();

        } else {
            references.addClass('is-active');
            referencesList.slideDown();
        }
    });

    $('.footnote h4').on('click', function() {
        var footnote = $(this).closest('.footnote');
        var footnoteList = $(this).next('p');
        if (footnote.hasClass('is-active')){
            footnote.removeClass('is-active');
            footnoteList.slideUp();

        } else {
            footnote.addClass('is-active');
            footnoteList.slideDown();
        }
    });


    $('.image_placeholder').on('click', function(){
       //alert('HALO');
       $(this).hide();
       $(this).next().show();
       var video = $('.video_player video');
       video.get(0).play();
    });

    // $( ".external" ).click( function( event ) {
    //     elementUrl = $( this ).attr( "href" );
    //
    //     event.preventDefault();
    //
    //     $( ".notification-box" ).addClass( "active" );
    // } );
    //
    // $( ".notification-box span" ).click( function() {
    //     if( $( this ).hasClass( "go" ) ) {
    //         window.location.href = elementUrl;
    //     }
    //
    //     $( ".notification-box" ).removeClass( "active" );
    //     console.log("qwe");
    // } );

    Cog.registerComponent({
        name: "body",
        api: api,
        selector: "body"
    });
}(Cog.jQuery(), _));