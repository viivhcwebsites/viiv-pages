(function ($) {
	
    var api = {},
        selectImage, currentTime, prevSlideTime = 0,
        slideTime, initialWidth, totalWidth = 0,
        finalWidth;
    api.onRegister = function (element) {
    	var $webinar = element.$scope;
        $(".webinar .videoDetail ul li:odd").addClass("odd");
        $(".webinar .videoDetail ul li:even").addClass("alpha");
        $(".webinar .imageBox :first").addClass('activeSlide');

        $webinar.each(function () {
            $(this).on("click", ".imageBox", api.showCurrentSilde);
            $(this).on("click", ".webinarArrowLeft", api.previousSlide);
            $(this).on("click", ".webinarArrowRight", api.nextSlide);
        });

        totalWidth = $(".webinar .imageBox").width();
        var count = 0;
        $(".webinar .imageBox").each(function (index) {
            count++;
        });
        $(".webinar .imagesContainer").css("width", count * totalWidth + "px");
        if($(".webinar .video-video-container").length>0){
       $(".webinar .video-video-container").find('video').on('timeupdate', function () { 
            currentTime = $(".webinar .video-video-container").find('video').get(0).currentTime; 
            $(".imageBox").each(function () {
                slideTime = $(this).attr("data-interval");
                prevSlideTime = $(this).prev().attr("data-interval");
                if (currentTime > slideTime) {
                    $(this).prev().removeClass("activeSlide");
                    $(this).addClass("activeSlide");
                    selectImage = $(this).find("img").attr("src");
                    //$(".webinar .slide").find("img").removeAttr("src");
                    $(".webinar .slide").find("img").attr("src", selectImage);
                }
            });
        });
    };
    }
    api.showCurrentSilde = function (event, triggered) {
        $(".imageBox").removeClass("activeSlide");
        var videoTime = $(this).attr("data-interval");
        $(this).addClass("activeSlide");
        selectImage = $(this).find("img").attr("src");
        $(".webinar .slide").find("img").removeAttr("src");
        $(".webinar .slide").find("img").attr("src", selectImage);
        $(".webinar .video-video-container").find('video').get(0).currentTime = videoTime;
        $(".webinar .video-video-container").find('video').get(0).play();  
    };
    api.previousSlide = function (event, triggered) {
        initialWidth = $(".imagesContainer").css("left");
        if (initialWidth != "0px") {
            $(".imagesContainer").animate({
                "left": "+=167px"
            });
        }
    };
    api.nextSlide = function (event, triggered) {
        event.preventDefault();
        initialWidth = $(".imagesContainer").css("left");
        var slideCount = 0;
        $(".webinar .imageBox").each(function (index) {
            slideCount++;
        });
        finalWidth = 0;
        while (slideCount > 5) {
            finalWidth = finalWidth - 167;
            slideCount--;
        }
        var temp = finalWidth + "px";

        if (initialWidth != temp) {

            $(".imagesContainer").animate({
                "left": "-=167px"
            });
        }
    };

Cog.registerComponent({
    name: "webinar",
    api: api,
    selector: ".webinar"
});
})(Cog.jQuery());