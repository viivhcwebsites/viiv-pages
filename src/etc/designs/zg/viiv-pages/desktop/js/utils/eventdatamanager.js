if (typeof window.GSKTECH === 'undefined') {
    window.GSKTECH = {};
}
if (typeof window.GSKTECH.CF === 'undefined') {
    window.GSKTECH.CF = {}
}

window.GSKTECH.CF.eventDataManager = (function ($) {

    var instance;

    function EventDataManager() {
        var eventDataManager = {};

        var cacheArray = [];

        eventDataManager.getCache = function (cacheKey) {
            var cacheObj = _.find(cacheArray, {key: cacheKey});
            return cacheObj ? cacheObj.data : null;
        };

        eventDataManager.addCache = function (cacheKey, cacheData) {
            if (eventDataManager.getCache(cacheKey) == null) {
                cacheArray.push({key: cacheKey, data: cacheData});
            } else {
                eventDataManager.clearCache(cacheKey);
                eventDataManager.addCache(cacheKey, cacheData);
            }
        };

        eventDataManager.clearCache = function (cacheKey) {
            if (cacheKey) {
                cacheArray = _.filter(cacheArray, function (element) {
                    return element.key !== cacheKey
                });
            } else {
                cacheArray = [];
            }
        };

        eventDataManager.getACMSdata = function (options) {
            var result = eventDataManager.getCache(options.cacheKey);

            if (!result) {
                result = $.ajax(options.request).promise();

                eventDataManager.addCache(options.cacheKey, result);
            }

            return result;
        };

        return eventDataManager;
    }

    return {
        getInstance: function () {
            if (!instance) {
                instance = new EventDataManager;
            }

            return instance;
        }
    };

})(Cog.jQuery());
